/*----------------------------------- 24-02-2020--------------------------------------------*/

ALTER TABLE `users`  ADD `forgotten_password_selector` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL  AFTER `forgotten_password_time`;

ALTER TABLE `users`  ADD `remember_selector` VARCHAR(150) NULL DEFAULT NULL  AFTER `forgotten_password_selector`;



ALTER TABLE `permissions` ADD `is_active` INT NOT NULL AFTER `perm_name`;
ALTER TABLE `users` ADD `revenue_district_id` INT NOT NULL AFTER `ip_address`;
