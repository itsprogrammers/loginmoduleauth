-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 29, 2020 at 10:30 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `logn_module`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `cat_name`, `slug`, `created_at`, `updated_at`) VALUES
(6, 'Wordpress', 'wordpress', '2018-06-12 10:00:20', '0000-00-00 00:00:00'),
(7, 'Web Design', 'web-design', '2018-06-21 10:02:16', '0000-00-00 00:00:00'),
(8, 'PhotoShop', 'photoshop', '2018-06-23 09:14:19', '0000-00-00 00:00:00'),
(9, 'Sports', 'sports', '2018-06-23 09:14:37', '0000-00-00 00:00:00'),
(10, 'Programming', 'programming', '2018-06-23 09:16:35', '0000-00-00 00:00:00'),
(11, 'News', 'news', '2018-07-16 08:36:58', '2018-07-16 05:36:58');

-- --------------------------------------------------------

--
-- Table structure for table `blog_post`
--

CREATE TABLE `blog_post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `meta_desc` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `featured_img` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `content` text NOT NULL,
  `deleted_at` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `allow_comments` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_post`
--

INSERT INTO `blog_post` (`id`, `title`, `category_id`, `user_id`, `slug`, `meta_desc`, `meta_keywords`, `featured_img`, `status`, `content`, `deleted_at`, `created_at`, `updated_at`, `allow_comments`) VALUES
(3, 'Our Security Policy has been Update', 7, 1, 'our-security-policy-has-been-update', 'dummy description', 'one,two', 'http://localhost/login/uploads/blog_images/ps4.jpg', 1, '<p>some content</p>', 'Null', '2018-06-23 02:52:59', '2018-06-23 06:12:56', 1),
(4, 'Laravel and Vue.js in action', 10, 1, 'laravel-and-vuejs-in-action', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Laravel, Php, Vue.js', 'http://localhost/login/uploads/blog_images/vue-spa-with-laravel.png', 1, '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\"><span style=\"font-family: Helvetica;\">L</span><span style=\"font-family: Helvetica;\">?</span><span style=\"font-family: Helvetica;\">orem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non sapien vulputate, convallis lectus et, commodo libero. Mauris varius nisl bibendum porta tincidunt. Suspendisse vitae dui efficitur, ullamcorper ligula eu, sagittis arcu. Curabitur euismod neque nec suscipit consectetur. Donec rutrum nec magna vel auctor. Duis sapien velit, pharetra sit amet urna et, consequat elementum velit. Integer eros eros, condimentum eget enim vel, consectetur euismod est. Nam sodales congue convallis. Donec nunc magna, suscipit eget posuere in, viverra a lorem.</span></p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\"><span style=\"font-family: Helvetica;\">Donec vel placerat massa. Integer nisi libero, venenatis vitae vulputate in, cursus ut magna. Suspendisse potenti. Sed sit amet venenatis turpis, vitae consequat est. Mauris vitae sem id erat gravida vestibulum. Nullam venenatis at quam eget pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet eleifend metus, non elementum enim. Nulla rhoncus, orci in commodo hendrerit, ipsum massa vulputate tellus, vitae tempor nibh nunc nec velit. Nullam viverra, turpis eu gravida rutrum, mi orci viverra lorem, id facilisis est turpis varius diam. Nunc sed tincidunt ligula, quis convallis lacus. Fusce facilisis faucibus lectus, a laoreet nunc facilisis eu. Mauris faucibus, purus ut facilisis ultrices, nibh diam luctus ex, et interdum eros mauris eget nulla. Mauris volutpat nibh at lectus ullamcorper aliquet. Fusce consectetur commodo eros, ut fringilla dolor tempor non. Etiam ac libero vel turpis commodo aliquet a quis est.</span></p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\"><span style=\"font-family: Helvetica;\">Donec ut odio pharetra, ullamcorper tellus non, luctus mi. Nulla imperdiet lorem tellus, vitae scelerisque lacus porta eget. Mauris vel odio dolor. Quisque fringilla at nulla id auctor. Nam molestie finibus tortor, ut dapibus nisi feugiat sed. Vestibulum gravida eros sed arcu consectetur, dictum interdum tortor condimentum. Aliquam consectetur elit a mi faucibus, a consectetur arcu vestibulum. Cras vel dui et mi pretium condimentum sed nec sapien. Vestibulum finibus volutpat massa.</span></p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\"><span style=\"font-family: Helvetica;\">Quisque porta massa id elementum facilisis. Vestibulum a imperdiet orci. Proin gravida odio a maximus fermentum. Mauris molestie eget nisl in ultrices. Phasellus quis risus velit. Donec finibus mi gravida mi gravida sagittis. Duis nec ullamcorper felis, eu posuere lectus. Ut tincidunt, quam eget aliquet egestas, erat enim pulvinar nisi, ac ultrices tellus nunc nec tellus. Sed vitae lacus vitae dui facilisis volutpat. Ut congue ex vitae purus dictum, nec elementum mi aliquam. Mauris elit diam, tempus quis feugiat sit amet, mattis et sapien. Sed eu dolor commodo, volutpat odio ac, viverra odio.</span></p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\"><span style=\"font-family: Helvetica;\">Integer mauris dolor, scelerisque quis lorem vitae, euismod convallis massa. Nulla consequat ligula at justo efficitur, in porta massa porta. Vivamus interdum sagittis diam, vitae ultricies diam sagittis ut. Quisque fringilla nunc id libero mollis facilisis. Nam eu scelerisque sapien, sed varius velit. Curabitur purus magna, finibus vitae suscipit eu, bibendum nec mi. Quisque neque neque, pulvinar at ullamcorper in, aliquet eu libero. Maecenas sed dapibus diam. Phasellus pretium scelerisque lectus, a molestie erat sodales in. Suspendisse euismod mollis neque, non lacinia eros viverra sit amet. Ut venenatis diam mollis sem interdum, id luctus dui pretium.</span></p>', 'Null', '2018-06-23 06:20:42', '2018-06-23 06:20:42', 1),
(5, 'Create Restfull API with Laravel 5.3', 7, 1, 'create-restfull-api-with-laravel-53', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non sapien vulputate, convallis lectus et,', 'Restful API, Laravel, Larave 5.3,API', 'http://localhost/login/uploads/blog_images/Laravel-51.png', 1, '<hr style=\"margin: 0px; padding: 0px; border-top: 0px; clear: both; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0)); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" center;\"=\"\"><div id=\"Content\" style=\"margin: 0px; padding: 0px; position: relative; font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" center;\"=\"\"><div class=\"boxed\" style=\"margin: 10px 28.7969px; padding: 0px; clear: both;\"><div id=\"lipsum\" style=\"margin: 0px; padding: 0px; text-align: justify;\"><p style=\"margin-bottom: 15px; padding: 0px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non sapien vulputate, convallis lectus et, commodo libero. Mauris varius nisl bibendum porta tincidunt. Suspendisse vitae dui efficitur, ullamcorper ligula eu, sagittis arcu. Curabitur euismod neque nec suscipit consectetur. Donec rutrum nec magna vel auctor. Duis sapien velit, pharetra sit amet urna et, consequat elementum velit. Integer eros eros, condimentum eget enim vel, consectetur euismod est. Nam sodales congue convallis. Donec nunc magna, suscipit eget posuere in, viverra a lorem.</p><p style=\"margin-bottom: 15px; padding: 0px;\"><br></p></div></div></div>', 'Null', '2018-06-23 06:24:44', '2018-08-04 02:34:18', 1),
(6, 'laravel vs codeigniter', 10, 1, 'laravel-vs-codeigniter', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non sapien vulputate, convallis lectus et, commodo libero.', 'Laravel, Codeigniter', 'http://localhost/login/uploads/blog_images/laravel-vs-codeigniter-2.jpg', 1, '<div id=\"Content\" style=\"margin: 0px; padding: 0px; position: relative; font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: center;\"><div class=\"boxed\" style=\"margin: 10px 28.7969px; padding: 0px; clear: both;\"><div id=\"lipsum\" style=\"margin: 0px; padding: 0px; text-align: justify;\"><p style=\"margin-bottom: 15px; padding: 0px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non sapien vulputate, convallis lectus et, commodo libero. Mauris varius nisl bibendum porta tincidunt. Suspendisse vitae dui efficitur, ullamcorper ligula eu, sagittis arcu. Curabitur euismod neque nec suscipit consectetur. Donec rutrum nec magna vel auctor. Duis sapien velit, pharetra sit amet urna et, consequat elementum velit. Integer eros eros, condimentum eget enim vel, consectetur euismod est. Nam sodales congue convallis. Donec nunc magna, suscipit eget posuere in, viverra a lorem.</p><p style=\"margin-bottom: 15px; padding: 0px;\">Donec vel placerat massa. Integer nisi libero, venenatis vitae vulputate in, cursus ut magna. Suspendisse potenti. Sed sit amet venenatis turpis, vitae consequat est. Mauris vitae sem id erat gravida vestibulum. Nullam venenatis at quam eget pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet eleifend metus, non elementum enim. Nulla rhoncus, orci in commodo hendrerit, ipsum massa vulputate tellus, vitae tempor nibh nunc nec velit. Nullam viverra, turpis eu gravida rutrum, mi orci viverra lorem, id facilisis est turpis varius diam. Nunc sed tincidunt ligula, quis convallis lacus. Fusce facilisis faucibus lectus, a laoreet nunc facilisis eu. Mauris faucibus, purus ut facilisis ultrices, nibh diam luctus ex, et interdum eros mauris eget nulla. Mauris volutpat nibh at lectus ullamcorper aliquet. Fusce consectetur commodo eros, ut fringilla dolor tempor non. Etiam ac libero vel turpis commodo aliquet a quis est.</p><p style=\"margin-bottom: 15px; padding: 0px;\">Donec ut odio pharetra, ullamcorper tellus non, luctus mi. Nulla imperdiet lorem tellus, vitae scelerisque lacus porta eget. Mauris vel odio dolor. Quisque fringilla at nulla id auctor. Nam molestie finibus tortor, ut dapibus nisi feugiat sed. Vestibulum gravida eros sed arcu consectetur, dictum interdum tortor condimentum. Aliquam consectetur elit a mi faucibus, a consectetur arcu vestibulum. Cras vel dui et mi pretium condimentum sed nec sapien. Vestibulum finibus volutpat massa.</p><p style=\"margin-bottom: 15px; padding: 0px;\">Quisque porta massa id elementum facilisis. Vestibulum a imperdiet orci. Proin gravida odio a maximus fermentum. Mauris molestie eget nisl in ultrices. Phasellus quis risus velit. Donec finibus mi gravida mi gravida sagittis. Duis nec ullamcorper felis, eu posuere lectus. Ut tincidunt, quam eget aliquet egestas, erat enim pulvinar nisi, ac ultrices tellus nunc nec tellus. Sed vitae lacus vitae dui facilisis volutpat. Ut congue ex vitae purus dictum, nec elementum mi aliquam. Mauris elit diam, tempus quis feugiat sit amet, mattis et sapien. Sed eu dolor commodo, volutpat odio ac, viverra odio.</p><p style=\"margin-bottom: 15px; padding: 0px;\">Integer mauris dolor, scelerisque quis lorem vitae, euismod convallis massa. Nulla consequat ligula at justo efficitur, in porta massa porta. Vivamus interdum sagittis diam, vitae ultricies diam sagittis ut. Quisque fringilla nunc id libero mollis facilisis. Nam eu scelerisque sapien, sed varius velit. Curabitur purus magna, finibus vitae suscipit eu, bibendum nec mi. Quisque neque neque, pulvinar at ullamcorper in, aliquet eu libero. Maecenas sed dapibus diam. Phasellus pretium scelerisque lectus, a molestie erat sodales in. Suspendisse euismod mollis neque, non lacinia eros viverra sit amet. Ut venenatis diam mollis sem interdum, id luctus dui pretium.</p></div></div></div>', 'Null', '2018-06-23 06:27:28', '2018-06-23 06:27:28', 1),
(7, 'Photoshop new version', 8, 1, 'photoshop-new-version', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non sapien vulputate, convallis lectus et, commodo libero.', 'photoshop, new-version,designing', 'http://localhost/login/uploads/blog_images/Photoshop.jpg', 1, '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non sapien vulputate, convallis lectus et, commodo libero. Mauris varius nisl bibendum porta tincidunt. Suspendisse vitae dui efficitur, ullamcorper ligula eu, sagittis arcu. Curabitur euismod neque nec suscipit consectetur. Donec rutrum nec magna vel auctor. Duis sapien velit, pharetra sit amet urna et, consequat elementum velit. Integer eros eros, condimentum eget enim vel, consectetur euismod est. Nam sodales congue convallis. Donec nunc magna, suscipit eget posuere in, viverra a lorem.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Donec vel placerat massa. Integer nisi libero, venenatis vitae vulputate in, cursus ut magna. Suspendisse potenti. Sed sit amet venenatis turpis, vitae consequat est. Mauris vitae sem id erat gravida vestibulum. Nullam venenatis at quam eget pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet eleifend metus, non elementum enim. Nulla rhoncus, orci in commodo hendrerit, ipsum massa vulputate tellus, vitae tempor nibh nunc nec velit. Nullam viverra, turpis eu gravida rutrum, mi orci viverra lorem, id facilisis est turpis varius diam. Nunc sed tincidunt ligula, quis convallis lacus. Fusce facilisis faucibus lectus, a laoreet nunc facilisis eu. Mauris faucibus, purus ut facilisis ultrices, nibh diam luctus ex, et interdum eros mauris eget nulla. Mauris volutpat nibh at lectus ullamcorper aliquet. Fusce consectetur commodo eros, ut fringilla dolor tempor non. Etiam ac libero vel turpis commodo aliquet a quis est.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Donec ut odio pharetra, ullamcorper tellus non, luctus mi. Nulla imperdiet lorem tellus, vitae scelerisque lacus porta eget. Mauris vel odio dolor. Quisque fringilla at nulla id auctor. Nam molestie finibus tortor, ut dapibus nisi feugiat sed. Vestibulum gravida eros sed arcu consectetur, dictum interdum tortor condimentum. Aliquam consectetur elit a mi faucibus, a consectetur arcu vestibulum. Cras vel dui et mi pretium condimentum sed nec sapien. Vestibulum finibus volutpat massa.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Quisque porta massa id elementum facilisis. Vestibulum a imperdiet orci. Proin gravida odio a maximus fermentum. Mauris molestie eget nisl in ultrices. Phasellus quis risus velit. Donec finibus mi gravida mi gravida sagittis. Duis nec ullamcorper felis, eu posuere lectus. Ut tincidunt, quam eget aliquet egestas, erat enim pulvinar nisi, ac ultrices tellus nunc nec tellus. Sed vitae lacus vitae dui facilisis volutpat. Ut congue ex vitae purus dictum, nec elementum mi aliquam. Mauris elit diam, tempus quis feugiat sit amet, mattis et sapien. Sed eu dolor commodo, volutpat odio ac, viverra odio.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Integer mauris dolor, scelerisque quis lorem vitae, euismod convallis massa. Nulla consequat ligula at justo efficitur, in porta massa porta. Vivamus interdum sagittis diam, vitae ultricies diam sagittis ut. Quisque fringilla nunc id libero mollis facilisis. Nam eu scelerisque sapien, sed varius velit. Curabitur purus magna, finibus vitae suscipit eu, bibendum nec mi. Quisque neque neque, pulvinar at ullamcorper in, aliquet eu libero. Maecenas sed dapibus diam. Phasellus pretium scelerisque lectus, a molestie erat sodales in. Suspendisse euismod mollis neque, non lacinia eros viverra sit amet. Ut venenatis diam mollis sem interdum, id luctus dui pretium.</p>', 'Null', '2018-06-23 06:30:29', '2018-06-23 06:30:29', 1),
(8, 'England made highest runs score in world cricket', 9, 1, 'england-made-highest-runs-score-in-world-cricket', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non sapien vulputate, convallis lectus et, commodo libero. Mauris varius nisl bibendum porta tincidunt.', 'england, cricket, sports,England -cricket', 'http://localhost/login/uploads/blog_images/skysports-jason-roy-roy-england_4340099.jpg', 1, '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non sapien vulputate, convallis lectus et, commodo libero. Mauris varius nisl bibendum porta tincidunt. Suspendisse vitae dui efficitur, ullamcorper ligula eu, sagittis arcu. Curabitur euismod neque nec suscipit consectetur. Donec rutrum nec magna vel auctor. Duis sapien velit, pharetra sit amet urna et, consequat elementum velit. Integer eros eros, condimentum eget enim vel, consectetur euismod est. Nam sodales congue convallis. Donec nunc magna, suscipit eget posuere in, viverra a lorem.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Donec vel placerat massa. Integer nisi libero, venenatis vitae vulputate in, cursus ut magna. Suspendisse potenti. Sed sit amet venenatis turpis, vitae consequat est. Mauris vitae sem id erat gravida vestibulum. Nullam venenatis at quam eget pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet eleifend metus, non elementum enim. Nulla rhoncus, orci in commodo hendrerit, ipsum massa vulputate tellus, vitae tempor nibh nunc nec velit. Nullam viverra, turpis eu gravida rutrum, mi orci viverra lorem, id facilisis est turpis varius diam. Nunc sed tincidunt ligula, quis convallis lacus. Fusce facilisis faucibus lectus, a laoreet nunc facilisis eu. Mauris faucibus, purus ut facilisis ultrices, nibh diam luctus ex, et interdum eros mauris eget nulla. Mauris volutpat nibh at lectus ullamcorper aliquet. Fusce consectetur commodo eros, ut fringilla dolor tempor non. Etiam ac libero vel turpis commodo aliquet a quis est.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Donec ut odio pharetra, ullamcorper tellus non, luctus mi. Nulla imperdiet lorem tellus, vitae scelerisque lacus porta eget. Mauris vel odio dolor. Quisque fringilla at nulla id auctor. Nam molestie finibus tortor, ut dapibus nisi feugiat sed. Vestibulum gravida eros sed arcu consectetur, dictum interdum tortor condimentum. Aliquam consectetur elit a mi faucibus, a consectetur arcu vestibulum. Cras vel dui et mi pretium condimentum sed nec sapien. Vestibulum finibus volutpat massa.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Quisque porta massa id elementum facilisis. Vestibulum a imperdiet orci. Proin gravida odio a maximus fermentum. Mauris molestie eget nisl in ultrices. Phasellus quis risus velit. Donec finibus mi gravida mi gravida sagittis. Duis nec ullamcorper felis, eu posuere lectus. Ut tincidunt, quam eget aliquet egestas, erat enim pulvinar nisi, ac ultrices tellus nunc nec tellus. Sed vitae lacus vitae dui facilisis volutpat. Ut congue ex vitae purus dictum, nec elementum mi aliquam. Mauris elit diam, tempus quis feugiat sit amet, mattis et sapien. Sed eu dolor commodo, volutpat odio ac, viverra odio.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Integer mauris dolor, scelerisque quis lorem vitae, euismod convallis massa. Nulla consequat ligula at justo efficitur, in porta massa porta. Vivamus interdum sagittis diam, vitae ultricies diam sagittis ut. Quisque fringilla nunc id libero mollis facilisis. Nam eu scelerisque sapien, sed varius velit. Curabitur purus magna, finibus vitae suscipit eu, bibendum nec mi. Quisque neque neque, pulvinar at ullamcorper in, aliquet eu libero. Maecenas sed dapibus diam. Phasellus pretium scelerisque lectus, a molestie erat sodales in. Suspendisse euismod mollis neque, non lacinia eros viverra sit amet. Ut venenatis diam mollis sem interdum, id luctus dui pretium.</p>', 'Null', '2018-06-23 06:35:36', '2018-06-23 06:35:36', 1),
(10, 'Ronalod Best Goals', 9, 1, 'ronalod-best-goals', 'Ronaldo Best Goals', 'football, Ronaldo,cr7', 'http://localhost/login/uploads/blog_images/skynews-ronaldo-portugal-football_4338029.jpg', 1, '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non sapien vulputate, convallis lectus et, commodo libero. Mauris varius nisl bibendum porta tincidunt. Suspendisse vitae dui efficitur, ullamcorper ligula eu, sagittis arcu. Curabitur euismod neque nec suscipit consectetur. Donec rutrum nec magna vel auctor. Duis sapien velit, pharetra sit amet urna et, consequat elementum velit. Integer eros eros, condimentum eget enim vel, consectetur euismod est. Nam sodales congue convallis. Donec nunc magna, suscipit eget posuere in, viverra a lorem.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Donec vel placerat massa. Integer nisi libero, venenatis vitae vulputate in, cursus ut magna. Suspendisse potenti. Sed sit amet venenatis turpis, vitae consequat est. Mauris vitae sem id erat gravida vestibulum. Nullam venenatis at quam eget pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sit amet eleifend metus, non elementum enim. Nulla rhoncus, orci in commodo hendrerit, ipsum massa vulputate tellus, vitae tempor nibh nunc nec velit. Nullam viverra, turpis eu gravida rutrum, mi orci viverra lorem, id facilisis est turpis varius diam. Nunc sed tincidunt ligula, quis convallis lacus. Fusce facilisis faucibus lectus, a laoreet nunc facilisis eu. Mauris faucibus, purus ut facilisis ultrices, nibh diam luctus ex, et interdum eros mauris eget nulla. Mauris volutpat nibh at lectus ullamcorper aliquet. Fusce consectetur commodo eros, ut fringilla dolor tempor non. Etiam ac libero vel turpis commodo aliquet a quis est.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Donec ut odio pharetra, ullamcorper tellus non, luctus mi. Nulla imperdiet lorem tellus, vitae scelerisque lacus porta eget. Mauris vel odio dolor. Quisque fringilla at nulla id auctor. Nam molestie finibus tortor, ut dapibus nisi feugiat sed. Vestibulum gravida eros sed arcu consectetur, dictum interdum tortor condimentum. Aliquam consectetur elit a mi faucibus, a consectetur arcu vestibulum. Cras vel dui et mi pretium condimentum sed nec sapien. Vestibulum finibus volutpat massa.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Quisque porta massa id elementum facilisis. Vestibulum a imperdiet orci. Proin gravida odio a maximus fermentum. Mauris molestie eget nisl in ultrices. Phasellus quis risus velit. Donec finibus mi gravida mi gravida sagittis. Duis nec ullamcorper felis, eu posuere lectus. Ut tincidunt, quam eget aliquet egestas, erat enim pulvinar nisi, ac ultrices tellus nunc nec tellus. Sed vitae lacus vitae dui facilisis volutpat. Ut congue ex vitae purus dictum, nec elementum mi aliquam. Mauris elit diam, tempus quis feugiat sit amet, mattis et sapien. Sed eu dolor commodo, volutpat odio ac, viverra odio.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Integer mauris dolor, scelerisque quis lorem vitae, euismod convallis massa. Nulla consequat ligula at justo efficitur, in porta massa porta. Vivamus interdum sagittis diam, vitae ultricies diam sagittis ut. Quisque fringilla nunc id libero mollis facilisis. Nam eu scelerisque sapien, sed varius velit. Curabitur purus magna, finibus vitae suscipit eu, bibendum nec mi. Quisque neque neque, pulvinar at ullamcorper in, aliquet eu libero. Maecenas sed dapibus diam. Phasellus pretium scelerisque lectus, a molestie erat sodales in. Suspendisse euismod mollis neque, non lacinia eros viverra sit amet. Ut venenatis diam mollis sem interdum, id luctus dui pretium.</p>', 'Null', '2018-06-23 07:09:52', '2018-06-23 07:09:52', 1),
(11, 'About President of pakistan', 9, 1, 'about-president-of-pakistan', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'kjhk,khkjh', 'http://localhost/login/uploads/blog_images/33868369_190291285125095_8752566590974722048_n_(1).jpg', 0, '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\"><b><u>Lorem ipsum dolor sit amet, consectetur adipiscing elit:</u></b></p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\">Praesent porta est at sapien ultrices, sit amet semper justo tempus. Nam convallis sapien nec tortor dignissim fermentum. Nunc dignissim tempor tortor, nec porttitor risus volutpat sit amet. Suspendisse luctus ullamcorper mi. Maecenas ultrices mi a ex congue, at dictum leo eleifend. In nisl nisl, ultricies ut metus a, ornare scelerisque nisl. Quisque dolor dui, suscipit sed rhoncus nec, accumsan et lorem. Donec eu bibendum nibh. Sed id elit urna. Curabitur pulvinar tristique lacus vel molestie. Praesent sit amet erat sed diam imperdiet lacinia ut ac magna. Aliquam ultricies, sem semper dictum placerat, nisl nunc iaculis felis, vel tincidunt tortor massa quis est. Integer finibus at risus id feugiat.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\"><b><u>Curabitur in velit vitae eros rutrum vulputate id quis velit:</u></b></p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\">Proin nunc massa, commodo vitae nisi eget, egestas efficitur mi. Morbi sit amet imperdiet sem. Suspendisse fermentum sem non nunc mattis, hendrerit convallis orci pellentesque. Integer tempus vel est sit amet rhoncus. Integer sed pretium libero, sed posuere elit. Donec vel varius mauris, ut sollicitudin dolor. Maecenas bibendum elit sit amet blandit scelerisque.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\"><b><u>Quisque nec nunc quam:</u></b></p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\">Cras condimentum pharetra mi et imperdiet. Duis consectetur mi sit amet ex sodales, ut interdum orci ornare. Mauris venenatis erat et nisl tincidunt dignissim. In hac habitasse platea dictumst. Nam suscipit nisi interdum faucibus dignissim. Aliquam porttitor lectus sit amet libero posuere finibus quis sit amet sapien. Nullam varius neque lectus, in aliquam neque sodales eleifend.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\"><b><u>Proin id justo eget justo venenatis tempus a sed leo.</u></b></p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\">Nam ut congue lorem. Vivamus enim justo, congue nec justo et, maximus elementum eros. Sed et tincidunt tellus. Nunc ultrices maximus massa sed ultricies. Donec faucibus hendrerit nulla. Mauris gravida orci eget nisi sodales, nec elementum libero dapibus. Cras lacus ante, ullamcorper eu tempor ut, dictum at mi. Aenean id vehicula tellus, sed varius diam. Etiam egestas convallis vulputate. Nullam nisi tellus, blandit ut nisl in, tincidunt placerat arcu. Morbi sed ultrices justo, eget mollis magna.</p>', '2018-08-04 07:30:49', '2018-07-14 12:37:37', '2018-07-30 12:09:25', 0),
(12, 'Nawaz Sharif\'s audio message from Adiala jail', 11, 1, 'nawaz-sharifs-audio-message-from-adiala-jail', 'LAHORE: Ousted prime minister Nawaz Sharif and Pakistan Muslim League-N (PML-N) Quaid Nawaz Sharif and his daughter Maryam Nawaz', 'nawaz,Maryam,maryam-nawaz', 'http://localhost/login/uploads/blog_images/l_342150_102047_updates.jpg', 1, '<p class=\"\" style=\"margin-top: 20px; margin-bottom: 20px; font-size: 18px; line-height: 27px; font-family: Merriweather;\">LAHORE: Ousted prime minister Nawaz Sharif and Pakistan Muslim League-N (PML-N) Quaid Nawaz Sharif and his daughter Maryam Nawaz, currently lodged in Rawalpindi\'s Adiala jail in a corruption case, have issued audio messages for the people of Pakistan.<br></p><p style=\"margin-top: 20px; margin-bottom: 20px; font-size: 18px; line-height: 27px; font-family: Merriweather;\">Addressing his party workers and leaders in his recorded message, Nawaz said he had been put in jail to keep him away from them. “My daughter, who’s a daughter of the nation, has also been lodged in jail,” he said adding that the opponents did not know that the prison and imprisonment could not sever his relationship with his people.</p><p style=\"margin-top: 20px; margin-bottom: 20px; font-size: 18px; line-height: 27px; font-family: Merriweather;\">“Neither any dictator could break off that relationship in the past nor those pulling the strings would be able to do so today,” asserted Nawaz Sharif. He requested the people to pray for recovery of his critically ill wife, whom he left behind, in a London hospital, to return to his country. He said the entire country had been turned into a jail. He said now was the time to stand up and break all shackles. He said the time had reached that people would have to free themselves from the (illegal) restrictions and to do away with the games being played with them for the last 70 years.</p><p style=\"margin-top: 20px; margin-bottom: 20px; font-size: 18px; line-height: 27px; font-family: Merriweather;\">He said such games with masses have turned the Quaid-e-Azam’s Pakistan into a laughing stock. “That’s why I have raised the slogan of ‘Vote Ko Izzat Do’ (Give Respect to Vote. Now it’s your turn to carry this flag (movement) forward.</p>', 'Null', '2018-07-16 05:44:44', '2018-07-16 05:44:44', 1),
(13, 'we are adding some new post', 6, 1, 'we-are-adding-some-new-post', 'i am adding new post', 'keyword1, keyword2', 'http://localhost/login/uploads/blog_images/Photoshop1.jpg', 1, '<p open=\"\" sans\",=\"\" arial,=\"\" sans-serif;\"=\"\" style=\"margin-bottom: 15px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: &quot;Open Sans&quot;; vertical-align: baseline; color: rgb(50, 50, 50); text-align: justify;\"><span style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\"><u style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit:</u></span></p><p open=\"\" sans\",=\"\" arial,=\"\" sans-serif;\"=\"\" style=\"margin-bottom: 15px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: &quot;Open Sans&quot;; vertical-align: baseline; color: rgb(50, 50, 50); text-align: justify;\">Praesent porta est at sapien ultrices, sit amet semper justo tempus. Nam convallis sapien nec tortor dignissim fermentum. Nunc dignissim tempor tortor, nec porttitor risus volutpat sit amet. Suspendisse luctus ullamcorper mi. Maecenas ultrices mi a ex congue, at dictum leo eleifend. In nisl nisl, ultricies ut metus a, ornare scelerisque nisl. Quisque dolor dui, suscipit sed rhoncus nec, accumsan et lorem. Donec eu bibendum nibh. Sed id elit urna. Curabitur pulvinar tristique lacus vel molestie. Praesent sit amet erat sed diam imperdiet lacinia ut ac magna. Aliquam ultricies, sem semper dictum placerat, nisl nunc iaculis felis, vel tincidunt tortor massa quis est. Integer finibus at risus id feugiat.</p><p open=\"\" sans\",=\"\" arial,=\"\" sans-serif;\"=\"\" style=\"margin-bottom: 15px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: &quot;Open Sans&quot;; vertical-align: baseline; color: rgb(50, 50, 50); text-align: justify;\"><span style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\"><u style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\">Curabitur in velit vitae eros rutrum vulputate id quis velit:</u></span></p><p open=\"\" sans\",=\"\" arial,=\"\" sans-serif;\"=\"\" style=\"margin-bottom: 15px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: &quot;Open Sans&quot;; vertical-align: baseline; color: rgb(50, 50, 50); text-align: justify;\">Proin nunc massa, commodo vitae nisi eget, egestas efficitur mi. Morbi sit amet imperdiet sem. Suspendisse fermentum sem non nunc mattis, hendrerit convallis orci pellentesque. Integer tempus vel est sit amet rhoncus. Integer sed pretium libero, sed posuere elit. Donec vel varius mauris, ut sollicitudin dolor. Maecenas bibendum elit sit amet blandit scelerisque.</p><p open=\"\" sans\",=\"\" arial,=\"\" sans-serif;\"=\"\" style=\"margin-bottom: 15px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: &quot;Open Sans&quot;; vertical-align: baseline; color: rgb(50, 50, 50); text-align: justify;\"><span style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\"><u style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\">Quisque nec nunc quam:</u></span></p><p open=\"\" sans\",=\"\" arial,=\"\" sans-serif;\"=\"\" style=\"margin-bottom: 15px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: &quot;Open Sans&quot;; vertical-align: baseline; color: rgb(50, 50, 50); text-align: justify;\">Cras condimentum pharetra mi et imperdiet. Duis consectetur mi sit amet ex sodales, ut interdum orci ornare. Mauris venenatis erat et nisl tincidunt dignissim. In hac habitasse platea dictumst. Nam suscipit nisi interdum faucibus dignissim. Aliquam porttitor lectus sit amet libero posuere finibus quis sit amet sapien. Nullam varius neque lectus, in aliquam neque sodales eleifend.</p>', 'Null', '2018-07-23 19:14:06', '2018-07-23 19:14:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_post_tags`
--

CREATE TABLE `blog_post_tags` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_post_tags`
--

INSERT INTO `blog_post_tags` (`id`, `post_id`, `tag_id`) VALUES
(10, 1, 1),
(11, 1, 4),
(12, 1, 5),
(13, 2, 2),
(14, 2, 4),
(15, 2, 6),
(18, 3, 1),
(19, 3, 2),
(20, 4, 10),
(21, 4, 11),
(24, 6, 12),
(25, 6, 14),
(26, 7, 10),
(27, 7, 11),
(28, 8, 8),
(29, 9, 14),
(30, 10, 4),
(31, 10, 7),
(35, 12, 11),
(36, 12, 12),
(37, 12, 13),
(44, 13, 4),
(45, 13, 7),
(46, 13, 8),
(47, 11, 4),
(48, 11, 7),
(49, 11, 8),
(53, 5, 12),
(54, 5, 13),
(55, 5, 14);

-- --------------------------------------------------------

--
-- Table structure for table `blog_settings`
--

CREATE TABLE `blog_settings` (
  `id` int(11) NOT NULL,
  `enable_comments` tinyint(4) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_kewords` text NOT NULL,
  `per_page_posts` int(10) NOT NULL,
  `default_img` text NOT NULL,
  `enable_category_posts` tinyint(4) NOT NULL,
  `enable_recent_post` tinyint(4) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `Linkedin` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_settings`
--

INSERT INTO `blog_settings` (`id`, `enable_comments`, `meta_description`, `meta_kewords`, `per_page_posts`, `default_img`, `enable_category_posts`, `enable_recent_post`, `youtube`, `Linkedin`, `facebook`) VALUES
(1, 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'sports,news,cricket,football', 8, 'http://localhost/login/uploads/blogging-SMB2.png', 1, 1, 'http://youtube.com/', 'https://linkedin.com/', 'http://facebook.com/');

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE `blog_tags` (
  `id` int(11) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `tag`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Articles', 'articles', '2018-06-12 09:59:38', '0000-00-00 00:00:00'),
(4, 'ronaldo', 'ronaldo', '2018-06-12 10:28:48', '0000-00-00 00:00:00'),
(7, 'football', 'football', '2018-06-23 09:15:15', '0000-00-00 00:00:00'),
(8, 'cricket', 'cricket', '2018-06-23 09:15:21', '0000-00-00 00:00:00'),
(9, 'messi', 'messi', '2018-06-23 09:15:25', '0000-00-00 00:00:00'),
(10, 'css', 'css', '2018-06-23 09:15:48', '0000-00-00 00:00:00'),
(11, 'bootstrap', 'bootstrap', '2018-06-23 09:15:54', '0000-00-00 00:00:00'),
(12, 'laravel', 'laravel', '2018-06-23 09:22:18', '0000-00-00 00:00:00'),
(13, 'laravel restfull api\'s', 'laravel-restfull-apis', '2018-06-23 09:22:34', '0000-00-00 00:00:00'),
(14, 'codeigniter', 'codeigniter', '2018-06-23 09:22:41', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

CREATE TABLE `captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `word` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(95, NULL, '::1', NULL),
(94, NULL, '::1', NULL),
(93, NULL, '::1', NULL),
(92, NULL, '::1', NULL),
(91, NULL, '::1', NULL),
(90, NULL, '::1', NULL),
(89, NULL, '::1', NULL),
(88, NULL, '::1', NULL),
(155, 1506493472, '::1', 'LZCV6ISP'),
(143, 1506492915, '::1', '7WxjqlFC'),
(144, 1506492932, '::1', 'Mr2fRDem'),
(145, 1506492959, '::1', 'lZdK1Yzg'),
(146, 1506492988, '::1', 'm8ezTko7'),
(147, 1506492992, '::1', 'fcJsHOil'),
(148, 1506493027, '::1', '9W1qhZzr'),
(149, 1506493090, '::1', 'oCm3xKTp'),
(150, 1506493112, '::1', 'ast0VMcS'),
(151, 1506493175, '::1', 'QT7vqdVC'),
(152, 1506493194, '::1', 'OPyn5qkH'),
(153, 1506493353, '::1', 'mc2WCAs3'),
(154, 1506493370, '::1', 'IJzTfuqg');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'DC', 'District user'),
(56, 'OA', 'Accountant');

-- --------------------------------------------------------

--
-- Table structure for table `group_perm`
--

CREATE TABLE `group_perm` (
  `id` int(10) NOT NULL,
  `group_id` int(10) NOT NULL,
  `perm_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_perm`
--

INSERT INTO `group_perm` (`id`, `group_id`, `perm_id`) VALUES
(487, 31, 10),
(488, 31, 11),
(489, 31, 12),
(542, 32, 10),
(543, 32, 11),
(544, 32, 14),
(545, 33, 13),
(546, 33, 14),
(547, 33, 15),
(548, 33, 16),
(549, 0, 10),
(550, 0, 11),
(551, 0, 13),
(568, 0, 10),
(569, 0, 11),
(575, 35, 10),
(590, 38, 10),
(591, 38, 11),
(592, 38, 12),
(593, 41, 12),
(594, 41, 13),
(595, 41, 14),
(596, 43, 10),
(597, 43, 11),
(598, 43, 12),
(599, 44, 16),
(611, 45, 17),
(622, 46, 10),
(623, 46, 11),
(624, 47, 10),
(625, 47, 11),
(626, 48, 10),
(627, 48, 11),
(628, 48, 12),
(629, 48, 14),
(630, 49, 11),
(631, 49, 12),
(635, 50, 10),
(636, 50, 13),
(660, 51, 12),
(661, 51, 13),
(662, 51, 16),
(663, 51, 17),
(702, 52, 10),
(703, 52, 11),
(704, 52, 17),
(725, 53, 10),
(726, 53, 11),
(727, 53, 14),
(752, 54, 10),
(753, 54, 16),
(754, 54, 17),
(783, 55, 12),
(940, 1, 10),
(941, 1, 11),
(942, 1, 12),
(943, 1, 13),
(944, 1, 14),
(945, 1, 17),
(946, 1, 21),
(947, 2, 12),
(948, 2, 17),
(949, 2, 21),
(952, 56, 17),
(953, 56, 21);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(22, '127.0.0.1', 'aadmin@admin.com', 1582950951);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `perm_id` int(10) UNSIGNED NOT NULL,
  `perm_name` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`perm_id`, `perm_name`, `is_active`) VALUES
(10, 'View Groups', 0),
(11, 'View Users', 0),
(12, 'Add Users', 0),
(13, 'Create Groups', 0),
(14, 'Permissions', 0),
(15, 'Setup', 1),
(16, 'Backup & Export Users', 1),
(17, 'Profile', 0),
(18, 'Social Login Setup', 1),
(19, 'email members', 1),
(20, 'blog', 1),
(21, 'Account Management', 0);

-- --------------------------------------------------------

--
-- Table structure for table `revenue_district_master`
--

CREATE TABLE `revenue_district_master` (
  `id` int(11) NOT NULL,
  `revenue_district_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `revenue_district_master`
--

INSERT INTO `revenue_district_master` (`id`, `revenue_district_name`) VALUES
(1, 'Thiruvananthapuram'),
(2, 'Kollam'),
(3, 'Pathanamthitta'),
(4, 'Alappuzha'),
(5, 'Kottayam'),
(6, 'Idukki'),
(7, 'Ernakulam'),
(8, 'Thrissur'),
(9, 'Palakkad'),
(10, 'Malappuram'),
(11, 'Kozhikode'),
(12, 'Wayanad'),
(13, 'Kannur'),
(14, 'Kasaragod');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `registration_status` tinyint(4) NOT NULL,
  `forgot_pass_status` tinyint(4) NOT NULL,
  `socail_login_status` tinyint(4) NOT NULL,
  `two_factor_auth` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `registration_status`, `forgot_pass_status`, `socail_login_status`, `two_factor_auth`) VALUES
(1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `oauth_provider` varchar(255) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `revenue_district_id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `forgotten_password_selector` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_selector` varchar(150) DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` varchar(255) DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `bio` text NOT NULL,
  `user_img` text NOT NULL,
  `verification_code` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `oauth_provider`, `ip_address`, `revenue_district_id`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `forgotten_password_selector`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `date`, `bio`, `user_img`, `verification_code`) VALUES
(1, '', '', 0, 'Administrator', '$2y$12$HNm0Lc7nvATYGhQy9GUiWOl.dJKindCJU144yS.LCpYpEzae0WMIG', '', 'admin@admin.com', NULL, NULL, NULL, NULL, NULL, NULL, 1268889823, '1582961346', 1, 'IT', 'Admin', 'ADMIN', '7994375694', '2017-01-26', '', 'kite_logo.jpg', 0),
(167, '', '127.0.0.1', 4, 'alpsc', '$2y$10$1EIGkkVufNoo2CM5xV3eU.bzoZnz99e40Ic4NHSb49PDPK32GU7PS', NULL, 'admww22in1234@fd.we', NULL, NULL, NULL, NULL, NULL, NULL, 1582966852, NULL, 1, 'ALP', 'DC', 'KITE', '2312312312', '2020-02-29', '', '', 0),
(166, '', '127.0.0.1', 1, 'optvm', '$2y$10$ws.bj3JaiiQ0FZfYhWeA8eQ9db1q7gCSxubD.AITiptrDaWFUyYKa', NULL, 'tvm@tvm.com', NULL, NULL, NULL, NULL, NULL, NULL, 1582960231, '1582961256', 1, 'Office Asst', 'TVM', 'KITE', '34234234234', '2020-02-29', '', 'kite_logo1.jpg', 0),
(165, '', '127.0.0.1', 1, 'dctvm', '$2y$10$kQiqCHkhDL.3GTu2108wneWIUnpnfaY1IpTlURG5/hWUNHMvX.8VW', NULL, 'dctvm@s.sdf', NULL, NULL, NULL, NULL, NULL, NULL, 1582960089, '1582968022', 1, 'DC TVM', 'DC', 'KITE', '12312312312312312312', '2020-02-29', '', 'kite_logo2.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(306, 122, 2),
(240, 83, 2),
(288, 102, 2),
(239, 1, 1),
(274, 105, 2),
(305, 121, 2),
(285, 109, 2),
(303, 119, 2),
(367, 167, 2),
(355, 158, 2),
(304, 120, 2),
(270, 101, 2),
(272, 103, 2),
(293, 112, 2),
(297, 116, 2),
(302, 118, 2),
(358, 161, 2),
(356, 159, 2),
(353, 156, 2),
(361, 164, 2),
(365, 166, 56),
(352, 155, 2),
(359, 162, 2),
(368, 165, 2),
(357, 160, 2),
(360, 163, 2),
(354, 157, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_post`
--
ALTER TABLE `blog_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_post_tags`
--
ALTER TABLE `blog_post_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_settings`
--
ALTER TABLE `blog_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_tags`
--
ALTER TABLE `blog_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_perm`
--
ALTER TABLE `group_perm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`perm_id`);

--
-- Indexes for table `revenue_district_master`
--
ALTER TABLE `revenue_district_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `blog_post`
--
ALTER TABLE `blog_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `blog_post_tags`
--
ALTER TABLE `blog_post_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `blog_settings`
--
ALTER TABLE `blog_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blog_tags`
--
ALTER TABLE `blog_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `group_perm`
--
ALTER TABLE `group_perm`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=954;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `perm_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `revenue_district_master`
--
ALTER TABLE `revenue_district_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=369;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
